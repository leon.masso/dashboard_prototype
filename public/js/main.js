//loading animation
$(window).on('load', function () {
    $('#loading').hide()
})

//prevent overlap from sidebar with topnav
let navHeight = $('#topNav').outerHeight()
$('#sidebar').css('padding-top', navHeight)


//choose profile in sidebar
$('.ClientProfile').click(function(e){
    //preventing the default link redirection
    e.preventDefault()

    $('#selectedProfile').val($(this).data('value'))
    $(this).closest('form').submit()
})


//Flag Emojis for Countrycodes
const getFlagEmoji = (countryCode) => countryCode.toUpperCase().replace(/./g,
    char => String.fromCodePoint(127397 + char.charCodeAt())
);
const flagReplace = document.querySelectorAll('[data-flag]');
flagReplace.forEach(s => s.innerHTML = getFlagEmoji(s.dataset.flag))


//Changing time period
minDate = new DateTime($('.startDate'), {
    format: 'YYYY-MM-DD'
})
maxDate = new DateTime($('.endDate'), {
    format: 'YYYY-MM-DD'
})
$('.dateInput').change(function(e){
    //preventing the default link redirection
    e.preventDefault()

    $('#dateForm').submit()
})


//datatable initialization and customization
let table =  $('.dataTable').DataTable( {
    //"dom": '<"toolbar">rtip',
    "dom": '<"toolbar"><"top">Brt<"bottom"lp><"clear">',
    "ordering": true,
    "info":     true,
    "scrollY":        "55vH",
    "scrollX":        true,
    "scrollCollapse": true,
    "paging":         true,
    stateSave: true,
    "order": [[ 0, "asc" ]],
    "lengthMenu": [[100, 250, 500, 1000, -1], [100, 250, 500, 1000, "All"]],
    columnDefs: [
        {
            targets: 0,
            className: 'noVis'
        }
    ],
    buttons: [
        {
            text: 'Custom Search',
            extend: 'searchBuilder',
            attr:  {
                id: 'custom-search-button'
            }
        },
        {
            text: 'Export PDF',
            extend: 'pdf'
        },
        {
            text: 'Export Excel',
            extend: 'excel'
        },
        {
            text: 'Visible Columns',
            extend: 'colvis',
            columns: ':not(.noVis)'
        }
    ],
    select: {
        style: 'multi'
    },
    //sum row in footer
    footerCallback: function () {
        let api = this.api()

        api.columns('.sum', { search: 'applied'}).every( function () {
            let sum = this.data().sum().toFixed(2)
            this.footer().innerHTML = sum
        } )

        api.columns('.count', { search: 'applied'}).every( function () {
            let count = this.data().count()
            this.footer().innerHTML = 'Count: ' + count
        } )

        api.columns('#acos', { search: 'applied'}).every( function () {
            let cost = api.column( '#cost', { 'search': 'applied' } ).data().sum()
            let sales = api.column( '#sales', { 'search': 'applied' } ).data().sum()
            if(cost == 0 && sales == 0){
                acos = 0.00
            }else{
                acos = ((cost/sales)*100).toFixed(2)
            }
            this.footer().innerHTML = acos
        } )
    }
} )

//datatable elements arrangement
$('.dateRange').appendTo( $("div.toolbar") )
//$('.searchField').appendTo( $("div.toolbar") )
$('.corEntity').appendTo( $("div.bottom") )
$('.dataTables_length').appendTo( $("div.bottom") )
$('.dataTables_length').css("display", "flex")
$('.dataTables_length').css("justify-content", "center")
$('select[name="DataTables_Table_0_length"]')
    .removeClass("custom-select custom-select-sm form-control form-control-sm")
    .addClass("custom-select custom-select-md form-control form-control-md")
table.buttons().container().css("display", "flex")
table.buttons().container().css("justify-content", "center")
table.buttons().container().appendTo( $("div.toolbar") )
$(".dt-buttons>button, .buttons-colvis").addClass("ml-3 rounded")

$(".dtsb-searchBuilder").addClass("bg-dark")

//get different entity data related to selected rows
function getRelatedData(){
    let data = table.column(0).rows( { selected: true } ).data()
    let entityArray=[];
    for (var i=0; i < data.length; i++){
        let anchorData = $(data[i][0]).attr('href')
        //let titleData = $(data[i][0]).text()
        entityArray.push(anchorData)
    }

    let entitySelection = $("[name = 'entitySelect']").val()
    let currentPage = $("[name = 'currentPage']").val()
    let selectedEntity = `/${entitySelection}`
    $("form#corEntityForm").attr('action', selectedEntity)


    let params = new URLSearchParams();
    params.append('entity', currentPage)

    params.append(currentPage, entityArray)

   let url = `${selectedEntity}?${params}`
    console.log(url)
    window.location.href = url
    //$('form#corEntityForm').submit()
}

function removeQueryParam(entity){
    let url = new URL(window.location.href)
    let params = new URLSearchParams(url.search)
    let entityType = params.get('entity')
    let entityParams = params.get(entityType)

    let entityParamsArray = entityParams.toString().split(',')
    entityParamsArray = entityParamsArray.filter(item => item !== entity)

    url.searchParams.delete(entityType)
    url.searchParams.set(entityType, entityParamsArray)

    if(entityParamsArray.length == 0){
        url.searchParams.delete('entity')
        url.searchParams.delete(entityType)
    }

    window.location.href = url.href
}