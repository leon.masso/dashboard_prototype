<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CampaignController;
use App\Http\Controllers\AdgroupController;
use App\Http\Controllers\KeywordController;
use App\Http\Controllers\OverviewController;
use App\Http\Controllers\TargetController;
use App\Http\Controllers\AsinController;
use App\Http\Controllers\skuController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\SearchtermController;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Home
Route::get('/', [OverviewController::class, 'index']);


//set Profile
Route::post('/setProfile', [Controller::class, 'setProfile']);

//set date
Route::post('/setDates', [Controller::class, 'setDates']);


//Overview
Route::get('/overview', [OverviewController::class, 'index']);

//Portfolios
Route::get('/portfolios', [PortfolioController::class, 'getPortfolios']);

//Campaigns
Route::get('/campaigns', [CampaignController::class, 'getCampaigns']);

//Ad Groups
Route::get('/adgroups', [AdgroupController::class, 'getAdgroups']);

//Targets
Route::get('/targets', [TargetController::class, 'getTargets']);

//Searchterms
Route::get('/searchterms', [SearchtermController::class, 'getSearchterms']);

//ASINs
Route::get('/asins', [AsinController::class, 'getAsins']);

//SKUs
Route::get('/skus', [skuController::class, 'getSkus']);
