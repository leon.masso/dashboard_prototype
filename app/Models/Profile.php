<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profiles";
    public $timestamps = false;

    /**
     * Get the campaigns for the profile.
     */
    public function campaigns()
    {
        return $this->hasMany(Campaign::class, 'profileid');
    }

    /**
     * Get the adgroups for the profile.
     */
    public function adgroups()
    {
        return $this->hasMany(Adgroup::class, 'profileid');
    }

    /**
     * Get the keywords for the profile.
     */
    public function keywords()
    {
        return $this->hasMany(Keyword::class, 'profileid');
    }

    /**
     * Get the targets for the profile.
     */
    public function targets()
    {
        return $this->hasMany(Target::class, 'profileid');
    }
}
