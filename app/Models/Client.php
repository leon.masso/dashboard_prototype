<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    /**
    * Get the profiles for the client.
    */
    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }

}
