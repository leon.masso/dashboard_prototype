<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    protected $table = "reports_targets_test";
    public $timestamps = false;

    /**
     * Get the profile that owns the keyword.
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profileid');
    }

    /**
     * Get the campaign that owns the keyword.
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaignid');
    }

    /**
     * Get the ad group that owns the keyword.
     */
    public function adGroup()
    {
        return $this->belongsTo(Adgroup::class, 'adgroupid');
    }
}
