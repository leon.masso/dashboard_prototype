<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Searchterm extends Model
{
    protected $table = "reports_targets_test";
    public $timestamps = false;

    /**
     * Get the campaign that owns the searchterm.
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaignid');
    }

    /**
     * Get the ad group that owns the searchterm.
     */
    public function adGroup()
    {
        return $this->belongsTo(Adgroup::class, 'adgroupid');
    }

    /**
     * Get the profile that owns the searchterm.
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profileid');
    }
}
