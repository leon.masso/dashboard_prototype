<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = "reports_campaigns_test";
    public $timestamps = false;

    /**
     * Get the profile that owns the campaign.
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profileid');
    }

    /**
     * Get the ad group for the campaign.
     */
    public function adgroups()
    {
        return $this->hasMany(Adgroup::class, 'campaignid');
    }

    /**
     * Get the ad group for the campaign.
     */
    public function asins()
    {
        return $this->hasMany(Productad::class, 'campaignid', 'campaignid');
    }
}
