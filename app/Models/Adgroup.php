<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adgroup extends Model
{
    protected $table = "reports_adgroups_test";
    public $timestamps = false;

    /**
     * Get the campaign that owns the ad group.
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaignid');
    }

    /**
     * Get the keywords for the ad group.
     */
    public function keywords()
    {
        return $this->hasMany(Keyword::class, 'adgroupid');
    }
}
