<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table = "portfolios";
    public $timestamps = false;

    /**
     * Get the profile that owns the campaign.
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profileid');
    }

    /**
     * Get the ad group for the campaign.
     */
    public function campaigns()
    {
        return $this->hasMany(Campaign::class, 'campaignid');
    }
}
