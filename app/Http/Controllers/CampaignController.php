<?php

namespace App\Http\Controllers;

use App\Models\Adgroup;
use App\Models\Productad;
use Illuminate\Http\Request;
use App\Models\Campaign;
use Illuminate\Support\Facades\DB;
use Session;

class CampaignController extends Controller
{
    public function getCampaigns(Request $req)
    {
        if(session()->has('currentProfile')){
            $profile = session()->get('currentProfile');
            foreach ($profile as $pr){
                $profileid = $pr->profileid;
            }
        }else{
            return redirect('/');
        }

        $startDate = session()->get('startDate');
        $endDate = session()->get('endDate');

        if($req->all() != NULL){
            $corEntityType = $req->input('entity');
            $corEntities = $req->input($corEntityType);
            $corEntitiesArray = explode(',', $corEntities);

            $relatedcampaignIdsArray = self::getRelatedEntityData($corEntityType, $corEntitiesArray, $profileid, $startDate, $endDate);

            $campaigReports = self::getEntityData('campaigns', $profileid, $startDate, $endDate, $relatedcampaignIdsArray);
            $campaigns = DB::table('campaigns', 'ed')
                ->where('profileid', $profileid)
                ->joinSub($campaigReports, 'rd', function ($join) {
                    $join->on('ed.campaignid', '=', 'rd.campaignid');
                })
                ->select('ed.campaignid', 'name', 'campaigntype', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->get();

            return view('Campaigns', ['profile' => $profile, 'campaigns' => $campaigns, 'startDate' => $startDate, 'endDate' => $endDate, 'corEntities' => $corEntitiesArray, 'corEntityType' => $corEntityType] );
        }

        else{
            $campaigReports = self::getEntityData('campaigns', $profileid, $startDate, $endDate);
            $campaigns = DB::table('campaigns', 'ed')
                ->where('profileid', $profileid)
                ->leftJoinSub($campaigReports, 'rd', function ($join) {
                    $join->on('ed.campaignid', '=', 'rd.campaignid');
                })
                ->select('ed.campaignid', 'name', 'campaigntype', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->get();

            return view('Campaigns', ['profile' => $profile, 'campaigns' => $campaigns, 'startDate' => $startDate, 'endDate' => $endDate] );
        }
    }
}