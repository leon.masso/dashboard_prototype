<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productad;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Route;

class AsinController extends Controller
{
    public function getAsins(Request $req)
    {
        if(session()->has('currentProfile')){
            $profile = session()->get('currentProfile');
            foreach ($profile as $pr){
                $profileid = $pr->profileid;
            }
        }else{
            return redirect('/');
        }

        $startDate = session()->get('startDate');
        $endDate = session()->get('endDate');

        if($req->all() != NULL){

            $corEntityType = $req->input('entity');
            $corEntities = $req->input($corEntityType);
            $corEntitiesArray = explode(',', $corEntities);

            $relatedcampaignIdsArray = self::getRelatedEntityData($corEntityType, $corEntitiesArray, $profileid, $startDate, $endDate);

            $asinReference = DB::table('reports_productads_test')
                ->selectRaw('asin')
                ->whereIn('campaignid', $relatedcampaignIdsArray);

            if($corEntityType == 'skus'){
                $asinReference->whereIn('sku', $corEntitiesArray);
            }
            else{
                $asinReference->whereIn('campaignid', $relatedcampaignIdsArray);
                }
            $asinReference = $asinReference->get();

            $asinReference= json_decode( json_encode($asinReference), true); //format array

            $asinReports = DB::table('reports_productads_test as rdt')
                ->join('campaigns as c', 'c.campaignid', '=', 'rdt.campaignid')
                ->where('rdt.profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->whereIn('asin', $asinReference)
                ->selectRaw('asin')
                ->selectRaw('count(distinct(rdt.campaignid)) as campaigns')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetric('orders'))
                ->selectRaw(self::calcMetric('sales'))
                ->selectRaw(self::calcMetric('acos'))
                ->groupByRaw('asin');

            $asins = DB::table('productads', 'ed')
                ->where('profileid', $profileid)
                ->joinSub($asinReports, 'rd', function ($join) {
                    $join->on('ed.asin', '=', 'rd.asin');
                })
                ->select('rd.asin', 'rd.campaigns', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->groupBy('rd.asin', 'rd.campaigns', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->get();

            return view('asins', ['profile' => $profile, 'asins' => $asins, 'startDate' => $startDate, 'endDate' => $endDate, 'corEntities' => $corEntitiesArray, 'corEntityType' => $corEntityType] );
        }

        else{
            $asinReports = DB::table('reports_productads_test as rdt')
                ->join('campaigns as c', 'c.campaignid', '=', 'rdt.campaignid')
                ->where('rdt.profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->selectRaw('asin')
                ->selectRaw('count(distinct(rdt.campaignid)) as campaigns')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetric('orders'))
                ->selectRaw(self::calcMetric('sales'))
                ->selectRaw(self::calcMetric('acos'))
                ->groupByRaw('asin');

            $asins = DB::table('productads', 'ed')
                ->where('profileid', $profileid)
                ->leftJoinSub($asinReports, 'rd', function ($join) {
                    $join->on('ed.asin', '=', 'rd.asin');
                })
                ->select('rd.asin', 'rd.campaigns', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->groupBy('rd.asin', 'rd.campaigns', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->get();

            return view('asins', ['profile' => $profile, 'asins' => $asins, 'startDate' => $startDate, 'endDate' => $endDate] );
        }
    }
}
