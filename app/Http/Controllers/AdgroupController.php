<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Adgroup;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Route;

class AdgroupController extends Controller
{
    public function getAdgroups(Request $req)
    {
        if(session()->has('currentProfile')){
            $profile = session()->get('currentProfile');
            foreach ($profile as $pr){
                $profileid = $pr->profileid;
            }
        }else{
            return redirect('/');
        }

        $startDate = session()->get('startDate');
        $endDate = session()->get('endDate');

        if($req->all() != NULL){
            $corEntityType = $req->input('entity');
            $corEntities = $req->input($corEntityType);
            $corEntitiesArray = explode(',', $corEntities);

            $relatedcampaignIdsArray = self::getRelatedEntityData($corEntityType, $corEntitiesArray, $profileid, $startDate, $endDate);

            $adgroupReports = self::getEntityData('adgroups', $profileid, $startDate, $endDate, $relatedcampaignIdsArray);
            $adgroups = DB::table('adgroups', 'ed')
                ->where('profileid', $profileid)
                ->joinSub($adgroupReports, 'rd', function ($join) {
                    $join->on('ed.adgroupid', '=', 'rd.adgroupid');
                })
                ->select('ed.campaignid', 'ed.name', 'ed.adgroupid', 'rd.campaigntype', 'rd.name as campaignname', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->get();

            return view('adgroups', ['profile' => $profile, 'adgroups' => $adgroups, 'startDate' => $startDate, 'endDate' => $endDate, 'corEntities' => $corEntitiesArray, 'corEntityType' => $corEntityType] );
        }

        else{
            $adgroupReports = self::getEntityData('adgroups', $profileid, $startDate, $endDate);
            $adgroups = DB::table('adgroups', 'ed')
                ->where('profileid', $profileid)
                ->leftJoinSub($adgroupReports, 'rd', function ($join) {
                    $join->on('ed.adgroupid', '=', 'rd.adgroupid');
                })
                ->select('ed.campaignid', 'ed.name', 'ed.adgroupid', 'rd.campaigntype', 'rd.name as campaignname', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->get();

            return view('adgroups', ['profile' => $profile, 'adgroups' => $adgroups, 'startDate' => $startDate, 'endDate' => $endDate] );
        }
    }
}
