<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Route;
use App\Models\Profile;
use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Models\Adgroup;
use App\Models\Keyword;
use App\Models\Target;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function setProfile(Request $req)
    {
        $profileInput = $req->input('selectedProfile');
        $profile  = Profile::query()->where('profileid', $profileInput)->where( [['status_id', 1], ['account_id', 1] ])->get();
        Session::put('currentProfile', $profile);

        return redirect('/');
    }

    public function setDates(Request $req){
        $startDate = $req->input('startDate');
        session()->put('startDate', $startDate);

        $endDate = $req->input('endDate');
        session()->put('endDate', $endDate);

        return redirect()->back();
    }

    public function getEntityData($entity, $profileid, $startDate, $endDate, $relatedcampaignIdsArray = NULL){

        switch($entity){
            case 'campaigns':
                $data = 'rd.campaignid';
                break;
            case 'adgroups':
                $data = 'c.name, c.campaigntype, rd.adgroupid';
                break;
        }

        $reportData = DB::table("reports_{$entity}_test", 'rd')
            ->where('rd.profileid', $profileid)
            ->join('campaigns as c', 'c.campaignid', '=', 'rd.campaignid')
            ->whereBetween('date', [$startDate, $endDate])
            ->selectRaw($data)
            ->selectRaw(self::calcMetric('impressions'))
            ->selectRaw(self::calcMetric('clicks'))
            ->selectRaw(self::calcMetric('cost'))
            ->selectRaw(self::calcMetric('orders'))
            ->selectRaw(self::calcMetric('sales'))
            ->selectRaw(self::calcMetric('acos'))
            ->groupByRaw($data);


            if(!empty($relatedcampaignIdsArray)){
                $reportData->whereIn('rd.campaignid', $relatedcampaignIdsArray);
            }

        $result = $reportData;

        return $result;
    }

    public function calcMetric($metric){
        switch ($metric){
            case 'impressions' :
                $metricData = 'coalesce( SUM(impressions), 0 ) as impressions';
                break;
            case 'clicks' :
                $metricData = 'coalesce( SUM(clicks), 0 ) as clicks';
                break;
            case 'cost' :
                $metricData = 'coalesce( SUM(cost), 0 ) as cost';
                break;
            case 'orders' :
                $metricData = 'SUM (
                case
                    when c.campaigntype = \'SB\' then coalesce(attributedconversions14d, 0)
                    when c.campaigntype = \'SDA\' and c.costtype = \'cpc\' then coalesce(attributedconversions14d, 0)
                    when c.campaigntype = \'SDA\' and c.costtype = \'vcpm\' then coalesce(viewattributedconversions14d, 0)
                    when c.campaigntype = \'SP\' then coalesce(attributedconversions7d, 0)
                end )
                as orders';
                break;
            case 'sales' :
                $metricData = 'SUM (
                case
                    when c.campaigntype = \'SB\' then coalesce(attributedsales14d, 0)
                    when c.campaigntype = \'SDA\' and c.costtype = \'cpc\' then coalesce(attributedsales14d, 0)
                    when c.campaigntype = \'SDA\' and c.costtype = \'vcpm\' then coalesce(viewattributedsales14d, 0)
                    when c.campaigntype = \'SP\' then coalesce(attributedsales7d, 0)
                end ) 
                as sales';
                break;
            case 'acos' :
                $metricData = 'cast(
                    (
                    (SUM(cost) / SUM (
                            case
                               when c.campaigntype = \'SB\' then NULLIF(attributedsales14d, 0)
                               when c.campaigntype = \'SDA\' and c.costtype = \'cpc\' then NULLIF(attributedsales14d, 0)
                               when c.campaigntype = \'SDA\' and c.costtype = \'vcpm\' then NULLIF(viewattributedsales14d, 0)
                               when c.campaigntype = \'SP\' then NULLIF(attributedsales7d, 0)
                            end
                        )
                    )* 100) as numeric(10, 2)
                )
                as acos';
                break;
        }

        return $metricData;
    }

    public function calcMetricNoCosttype($metric){
        switch ($metric){
            case 'orders' :
                $metricData = 'SUM (
                case
                    when c.campaigntype = \'SB\' then coalesce(attributedconversions14d, 0)
                    when c.campaigntype = \'SP\' then coalesce(attributedconversions7d, 0)
                end )
                as orders';
                break;
            case 'sales' :
                $metricData = 'SUM (
                case
                    when c.campaigntype = \'SB\' then coalesce(attributedsales14d, 0)
                    when c.campaigntype = \'SP\' then coalesce(attributedsales7d, 0)
                end )
                as sales';
                break;
            case 'acos' :
                $metricData = 'cast(
                    (
                    (SUM(cost) / SUM (
                            case
                               when c.campaigntype = \'SB\' then NULLIF(attributedsales14d, 0)
                               when c.campaigntype = \'SP\' then NULLIF(attributedsales7d, 0)
                            end
                        )
                    )* 100) as numeric(10, 2)
                )
                as acos';
                break;
        }

        return $metricData;
    }

    public function getRelatedEntityData($corEntityType, $corEntitiesArray, $profileid, $startDate, $endDate){

        if($corEntityType == 'asins'){$searchParam = 'asin';}
        else if($corEntityType == 'skus'){$searchParam = 'sku';}
        else if($corEntityType == 'adgroups'){$searchParam = 'adgroupid';}
        else if($corEntityType == 'campaigns'){$searchParam = 'campaignid';}

        $tablename = $corEntityType;

        if($corEntityType == 'asins' || $corEntityType == 'skus'){
            $tablename = 'productads';
        }
        else if($corEntityType == 'searchterms'){
            $tablename = 'targets';
        }

        if($corEntityType == 'portfolios'){
            $relatedcampaignIds = DB::table('portfolios');
        }
        else{
            $relatedcampaignIds = DB::table('reports_'.$tablename.'_test');
        }

        if($corEntityType == 'portfolios'){
            $relatedcampaignIds
                ->where('portfolios.profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->whereIn('portfolios.portfolioid', $corEntitiesArray)
                ->leftJoin('campaigns', 'portfolios.portfolioid', '=', 'campaigns.portfolioid')
                ->leftJoin('reports_campaigns_test', 'reports_campaigns_test.campaignid', '=', 'campaigns.campaignid')
                ->selectRaw("string_agg(reports_campaigns_test.campaignid, ',') AS campaignids");
        }
        else if($corEntityType == 'targets'){
            $keywords = DB::table('reports_keywords_test')
                ->where('profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->whereIn('keywordid', $corEntitiesArray)
                ->selectRaw("string_agg(distinct(campaignid), ',') AS campaignids")
                ->whereNull('query');

            $relatedcampaignIds
                ->whereNull('query')
                ->whereIn('targetid', $corEntitiesArray)
                ->selectRaw("string_agg(campaignid, ',') AS campaignids")
                ->union($keywords);
        }
        else if($corEntityType == 'searchterms'){
            $keywords = DB::table('reports_keywords_test')
                ->where('profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->whereIn('keywordid', $corEntitiesArray)
                ->selectRaw("string_agg(distinct(campaignid), ',') AS campaignids")
                ->whereNotNull('query');

            $relatedcampaignIds
                ->whereNotNull('query')
                ->whereIn('targetid', $corEntitiesArray)
                ->selectRaw("string_agg(campaignid, ',') AS campaignids")
                ->union($keywords);
        }
        else {
            $relatedcampaignIds
                ->where('profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->whereIn($searchParam, $corEntitiesArray)
                ->selectRaw("string_agg(campaignid, ',') AS campaignids");
        }

        $result = $relatedcampaignIds->get();

        $relatedcampaignIdsArray = array();
        foreach ($result as $r){
            $individualCampaignIdsArray = explode(',', $r->campaignids);
            foreach($individualCampaignIdsArray as $individualCampaignId){
                $relatedcampaignIdsArray[] = $individualCampaignId;
            }
        }

        return $relatedcampaignIdsArray;
    }
}