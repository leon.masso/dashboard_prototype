<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class OverviewController extends Controller
{

    public function index()
    {
        if (session()->has('currentProfile')) {
            $profile = session('currentProfile');
            return view('Overview', ['message' => '', 'profile' => $profile] );
        }
        else{
            $message =  "Please choose a Marketplace :)";
            return view('Overview', ['message' => $message, 'profile' => ''] );
        }
    }
}