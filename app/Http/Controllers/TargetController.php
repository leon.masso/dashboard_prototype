<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Target;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Route;

class TargetController extends Controller
{
    public function getTargets(Request $req)
    {
        if(session()->has('currentProfile')){
            $profile = session()->get('currentProfile');
            foreach ($profile as $pr){
                $profileid = $pr->profileid;
            }
        }else{
            return redirect('/');
        }

        $startDate = session()->get('startDate');
        $endDate = session()->get('endDate');

        if($req->all() != NULL){
            $corEntityType = $req->input('entity');
            $corEntities = $req->input($corEntityType);
            $corEntitiesArray = explode(',', $corEntities);

            $relatedcampaignIdsArray = self::getRelatedEntityData($corEntityType, $corEntitiesArray, $profileid, $startDate, $endDate);

            $keywordReports = DB::table('reports_keywords_test as rdt')
                ->join('campaigns as c', 'c.campaignid', '=', 'rdt.campaignid')
                ->where('rdt.profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->whereNull('query')
                ->selectRaw('keywordid, query')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetricNoCosttype('orders'))
                ->selectRaw(self::calcMetricNoCosttype('sales'))
                ->selectRaw(self::calcMetricNoCosttype('acos'))
                ->groupByRaw('keywordid, query');

            $keywords = DB::table('keywords', 'ed')
                ->join('campaigns as ced', 'ced.campaignid', '=', 'ed.campaignid')
                ->where('ed.profileid', $profileid)
                ->leftJoinSub($keywordReports, 'rd', function ($join) {
                    $join->on('ed.keywordid', '=', 'rd.keywordid');
                })
                ->whereIn('ced.campaignid', $relatedcampaignIdsArray)
                ->select('ed.keywordid', 'ed.keywordtext', 'ced.name as campaignname', 'ced.campaigntype', 'rd.query', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->groupBy('ed.keywordid', 'ed.keywordtext', 'ced.name', 'ced.campaigntype', 'rd.query', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos');



            $targetReports = DB::table('reports_targets_test as rdt')
                ->join('campaigns as c', 'c.campaignid', '=', 'rdt.campaignid')
                ->where('rdt.profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->whereNull('query')
                ->selectRaw('targetid, query')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetricNoCosttype('orders'))
                ->selectRaw(self::calcMetricNoCosttype('sales'))
                ->selectRaw(self::calcMetricNoCosttype('acos'))
                ->groupByRaw('targetid, query');

            $targets = DB::table('targets', 'ed')
                ->join('campaigns as ced', 'ced.campaignid', '=', 'ed.campaignid')
                ->where('ed.profileid', $profileid)
                ->leftJoinSub($targetReports, 'rd', function ($join) {
                    $join->on('ed.targetid', '=', 'rd.targetid');
                })
                ->whereIn('ced.campaignid', $relatedcampaignIdsArray)
                ->select('ed.targetid', 'ed.targetingexpression', 'ced.name as campaignname', 'ced.campaigntype', 'rd.query', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->groupBy('ed.targetid', 'ed.targetingexpression', 'ced.name', 'ced.campaigntype', 'rd.query', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->union($keywords)
                ->get();


            return view('Targets', ['profile' => $profile, 'targets' => $targets, 'startDate' => $startDate, 'endDate' => $endDate, 'corEntities' => $corEntitiesArray, 'corEntityType' => $corEntityType] );
        }

        else{
            $keywordReports = DB::table('reports_keywords_test as rdt')
                ->join('campaigns as c', 'c.campaignid', '=', 'rdt.campaignid')
                ->where('rdt.profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->whereNull('query')
                ->selectRaw('keywordid, query')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetricNoCosttype('orders'))
                ->selectRaw(self::calcMetricNoCosttype('sales'))
                ->selectRaw(self::calcMetricNoCosttype('acos'))
                ->groupByRaw('keywordid, query');

            $keywords = DB::table('keywords', 'ed')
                ->join('campaigns as ced', 'ced.campaignid', '=', 'ed.campaignid')
                ->where('ed.profileid', $profileid)
                ->leftJoinSub($keywordReports, 'rd', function ($join) {
                    $join->on('ed.keywordid', '=', 'rd.keywordid');
                })
                ->select('ed.keywordid', 'ed.keywordtext', 'ced.name as campaignname', 'ced.campaigntype', 'rd.query', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->groupBy('ed.keywordid', 'ed.keywordtext', 'ced.name', 'ced.campaigntype', 'rd.query', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos');



            $targetReports = DB::table('reports_targets_test as rdt')
                ->join('campaigns as c', 'c.campaignid', '=', 'rdt.campaignid')
                ->where('rdt.profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->whereNull('query')
                ->selectRaw('targetid, query')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetricNoCosttype('orders'))
                ->selectRaw(self::calcMetricNoCosttype('sales'))
                ->selectRaw(self::calcMetricNoCosttype('acos'))
                ->groupByRaw('targetid, query');

            $targets = DB::table('targets', 'ed')
                ->join('campaigns as ced', 'ced.campaignid', '=', 'ed.campaignid')
                ->where('ed.profileid', $profileid)
                ->leftJoinSub($targetReports, 'rd', function ($join) {
                    $join->on('ed.targetid', '=', 'rd.targetid');
                })
                ->select('ed.targetid', 'ed.targetingexpression', 'ced.name as campaignname', 'ced.campaigntype', 'rd.query', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->groupBy('ed.targetid', 'ed.targetingexpression', 'ced.name', 'ced.campaigntype', 'rd.query', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->union($keywords)
                ->get();

            return view('Targets', ['profile' => $profile, 'targets' => $targets, 'startDate' => $startDate, 'endDate' => $endDate] );
        }
    }
}
