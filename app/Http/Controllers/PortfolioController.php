<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Portfolio;
use Illuminate\Support\Facades\DB;
use Session;

class PortfolioController extends Controller
{
    public function getPortfolios(Request $req)
    {
        if(session()->has('currentProfile')){
            $profile = session()->get('currentProfile');
            foreach ($profile as $pr){
                $profileid = $pr->profileid;
            }
        }else{
            return redirect('/');
        }

        $startDate = session()->get('startDate');
        $endDate = session()->get('endDate');

        if($req->all() != NULL){
            $corEntityType = $req->input('entity');
            $corEntities = $req->input($corEntityType);
            $corEntitiesArray = explode(',', $corEntities);

            $relatedcampaignIdsArray = self::getRelatedEntityData($corEntityType, $corEntitiesArray, $profileid, $startDate, $endDate);

            $portfolioReference = Portfolio::whereIn('reports_campaigns_test.campaignid', $relatedcampaignIdsArray)
                ->leftJoin('campaigns', 'portfolios.portfolioid', '=', 'campaigns.portfolioid')
                ->leftJoin('reports_campaigns_test', 'reports_campaigns_test.campaignid', '=', 'campaigns.campaignid')
                ->selectRaw('portfolios.portfolioid')
                ->get();

            $portfolioReports = Portfolio::where('portfolios.profileid', $profileid)
                ->leftJoin('campaigns', 'portfolios.portfolioid', '=', 'campaigns.portfolioid')
                ->leftJoin('reports_campaigns_test as c', 'c.campaignid', '=', 'campaigns.campaignid')
                ->whereBetween('date', [$startDate, $endDate])
                ->whereIn('portfolios.portfolioid', $portfolioReference)
                ->selectRaw('portfolios.name, portfolios.portfolioid')
                ->selectRaw('count(distinct(c.campaignid)) as campaigns')
                ->selectRaw("string_agg(distinct(c.campaignid), ',') AS campaignids")
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetric('orders'))
                ->selectRaw(self::calcMetric('sales'))
                ->selectRaw(self::calcMetric('acos'))
                ->groupByRaw('portfolios.name, portfolios.portfolioid');

            $portfolios = Portfolio::where('portfolios.profileid', $profileid)
                ->joinSub($portfolioReports, 'rd', function ($join) {
                    $join->on('portfolios.portfolioid', '=', 'rd.portfolioid');
                })
                ->select('portfolios.portfolioid', 'portfolios.name', 'rd.campaigns', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->get();

            return view('Portfolios', ['profile' => $profile, 'portfolios' => $portfolios, 'startDate' => $startDate, 'endDate' => $endDate, 'corEntities' => $corEntitiesArray, 'corEntityType' => $corEntityType] );
        }

        else{
            $portfolioReports = Portfolio::where('portfolios.profileid', $profileid)
                ->leftJoin('campaigns', 'portfolios.portfolioid', '=', 'campaigns.portfolioid')
                ->leftJoin('reports_campaigns_test as c', 'c.campaignid', '=', 'campaigns.campaignid')
                ->whereBetween('date', [$startDate, $endDate])
                ->selectRaw('portfolios.name, portfolios.portfolioid')
                ->selectRaw('count(distinct(c.campaignid)) as campaigns')
                ->selectRaw("string_agg(distinct(c.campaignid), ',') AS campaignids")
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetric('orders'))
                ->selectRaw(self::calcMetric('sales'))
                ->selectRaw(self::calcMetric('acos'))
                ->groupByRaw('portfolios.name, portfolios.portfolioid');

            $portfolios = Portfolio::where('portfolios.profileid', $profileid)
                ->leftJoinSub($portfolioReports, 'rd', function ($join) {
                    $join->on('portfolios.portfolioid', '=', 'rd.portfolioid');
                })
                ->select('portfolios.portfolioid', 'portfolios.name', 'rd.campaigns', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->get();

            return view('Portfolios', ['profile' => $profile, 'portfolios' => $portfolios, 'startDate' => $startDate, 'endDate' => $endDate] );
        }
    }
}