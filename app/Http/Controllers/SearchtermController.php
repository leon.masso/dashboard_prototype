<?php

namespace App\Http\Controllers;

use App\Models\Keyword;
use Illuminate\Http\Request;
use App\Models\Searchterm;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Route;

class SearchtermController extends Controller
{
    public function getSearchterms(Request $req)
    {
        if(session()->has('currentProfile')){
            $profile = session()->get('currentProfile');
            foreach ($profile as $pr){
                $profileid = $pr->profileid;
            }
        }else{
            return redirect('/');
        }

        $startDate = session()->get('startDate');
        $endDate = session()->get('endDate');

        if($req->all() != NULL){
            $corEntityType = $req->input('entity');
            $corEntities = $req->input($corEntityType);
            $corEntitiesArray = explode(',', $corEntities);

            $relatedcampaignIdsArray = self::getRelatedEntityData($corEntityType, $corEntitiesArray, $profileid, $startDate, $endDate);

            $keywords = DB::table('reports_keywords_test')
                ->where('profileid', $profileid)
                ->whereNotNull('query')
                ->whereBetween('date', [$startDate, $endDate])
                ->whereIn('campaignid', $relatedcampaignIdsArray)
                ->selectRaw('keywordid, keywordtext, query, campaignid, campaignname, campaigntype, adgroupid, adgroupname')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetricNoCosttype('orders'))
                ->selectRaw(self::calcMetricNoCosttype('sales'))
                ->selectRaw(self::calcMetricNoCosttype('acos'))
                ->groupByRaw('keywordid, keywordtext, query, campaignid, campaignname, campaigntype, adgroupid, adgroupname');

            $searchterms = DB::table('reports_targets_test')
                ->where('profileid', $profileid)
                ->whereNotNull('query')
                ->whereBetween('date', [$startDate, $endDate])
                ->whereIn('campaignid', $relatedcampaignIdsArray)
                ->selectRaw('targetid, targetingexpression as target, query as searchterm, campaignid, campaignname, campaigntype, adgroupid, adgroupname')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetric('orders'))
                ->selectRaw(self::calcMetric('sales'))
                ->selectRaw(self::calcMetric('acos'))
                ->groupByRaw('targetid, targetingexpression, query, campaignid, campaignname, campaigntype, adgroupid, adgroupname')
                ->union($keywords)
                ->get();

            return view('Searchterms', ['profile' => $profile, 'searchterms' => $searchterms, 'startDate' => $startDate, 'endDate' => $endDate, 'corEntities' => $corEntitiesArray, 'corEntityType' => $corEntityType] );
        }

        else{
            $keywords = DB::table('reports_keywords_test')
                ->where('profileid', $profileid)
                ->whereNotNull('query')
                ->whereBetween('date', [$startDate, $endDate])
                ->selectRaw('keywordid, keywordtext, query, campaignname, campaigntype, adgroupid, adgroupname')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetricNoCosttype('orders'))
                ->selectRaw(self::calcMetricNoCosttype('sales'))
                ->selectRaw(self::calcMetricNoCosttype('acos'))
                ->groupByRaw('keywordid, keywordtext, query, campaignname, campaigntype, adgroupid, adgroupname');

            $searchterms = DB::table('reports_targets_test as rt')
                ->where('rt.profileid', $profileid)
                ->join('campaigns as c', 'c.campaignid', '=', 'rt.campaignid')
                ->whereNotNull('query')
                ->whereBetween('date', [$startDate, $endDate])
                ->selectRaw('targetid, targetingexpression as target, query as searchterm, rt.campaignname, rt.campaigntype, adgroupid, adgroupname')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetric('orders'))
                ->selectRaw(self::calcMetric('sales'))
                ->selectRaw(self::calcMetric('acos'))
                ->groupByRaw('targetid, targetingexpression, query, rt.campaignname, rt.campaigntype, adgroupid, adgroupname')
                ->union($keywords)
                ->get();

            return view('Searchterms', ['profile' => $profile, 'searchterms' => $searchterms, 'startDate' => $startDate, 'endDate' => $endDate] );
        }
    }
}
