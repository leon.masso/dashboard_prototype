<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productad;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Route;

class skuController extends Controller
{
    public function getSkus(Request $req)
    {
        if(session()->has('currentProfile')){
            $profile = session()->get('currentProfile');
            foreach ($profile as $pr){
                $profileid = $pr->profileid;
            }
        }else{
            return redirect('/');
        }

        $startDate = session()->get('startDate');
        $endDate = session()->get('endDate');

        if($req->all() != NULL){

            $corEntityType = $req->input('entity');
            $corEntities = $req->input($corEntityType);
            $corEntitiesArray = explode(',', $corEntities);

            $relatedcampaignIdsArray = self::getRelatedEntityData($corEntityType, $corEntitiesArray, $profileid, $startDate, $endDate);


            if($corEntityType == 'asins'){
                $asins = $corEntitiesArray;
            }
            else{
                $asins = DB::table('reports_productads_test')
                    ->where('profileid', $profileid)
                    ->whereBetween('date', [$startDate, $endDate])
                    ->whereIn('campaignid', $relatedcampaignIdsArray)
                    ->selectRaw('asin')
                    ->groupByRaw('asin')
                    ->get();

                $asins= json_decode( json_encode($asins), true); //format array
            }

            $skuReports = DB::table('reports_productads_test as rdt')
                ->join('campaigns as c', 'c.campaignid', '=', 'rdt.campaignid')
                ->where('rdt.profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->whereIn('asin', $asins)
                ->selectRaw('sku')
                ->selectRaw('count(distinct(rdt.campaignid)) as campaigns')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetric('orders'))
                ->selectRaw(self::calcMetric('sales'))
                ->selectRaw(self::calcMetric('acos'))
                ->groupByRaw('sku');

            $skus = DB::table('productads', 'ed')
                ->where('profileid', $profileid)
                ->joinSub($skuReports, 'rd', function ($join) {
                    $join->on('ed.sku', '=', 'rd.sku');
                })
                ->select('ed.sku', 'rd.campaigns', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->groupBy('ed.sku', 'rd.campaigns', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->get();

            return view('skus', ['profile' => $profile, 'skus' => $skus, 'startDate' => $startDate, 'endDate' => $endDate, 'corEntities' => $corEntitiesArray, 'corEntityType' => $corEntityType] );
        }
        else{
            $skuReports = DB::table('reports_productads_test as rdt')
                ->join('campaigns as c', 'c.campaignid', '=', 'rdt.campaignid')
                ->where('rdt.profileid', $profileid)
                ->whereBetween('date', [$startDate, $endDate])
                ->selectRaw('sku')
                ->selectRaw('count(distinct(rdt.campaignid)) as campaigns')
                ->selectRaw(self::calcMetric('impressions'))
                ->selectRaw(self::calcMetric('clicks'))
                ->selectRaw(self::calcMetric('cost'))
                ->selectRaw(self::calcMetric('orders'))
                ->selectRaw(self::calcMetric('sales'))
                ->selectRaw(self::calcMetric('acos'))
                ->groupByRaw('sku');

            $skus = DB::table('productads', 'ed')
                ->where('profileid', $profileid)
                ->leftJoinSub($skuReports, 'rd', function ($join) {
                    $join->on('ed.sku', '=', 'rd.sku');
                })
                ->select('ed.sku', 'rd.campaigns', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->groupBy('ed.sku', 'rd.campaigns', 'rd.impressions', 'rd.clicks', 'rd.cost', 'rd.orders', 'rd.sales', 'rd.acos')
                ->get();

            return view('skus', ['profile' => $profile, 'skus' => $skus, 'startDate' => $startDate, 'endDate' => $endDate] );
        }
    }
}
