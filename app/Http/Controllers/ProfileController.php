<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function getOverview(Profile $profile)
    {
        return view('overview', ['profile' => $profile] );
    }
}
