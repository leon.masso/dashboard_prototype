<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Profile;
use ConsoleTVs\Charts\Registrar as Charts;
use Illuminate\Support\Facades\DB;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.master', function ($view) {
            $profiles = Profile::where( [['status_id', 1], ['account_id', 1] ])
                ->orderBy('name', 'ASC')
                ->get();

            $view->with('profiles', $profiles);
        });

        view()->composer('layouts.master', function ($view) {
            if(session()->has('currentProfile')){
                $currentProfile = session()->get('currentProfile');
            }else{
                $currentProfile = [''];
            }

            $view->with('currentProfile', $currentProfile);
        });

        view()->composer('components.dateSelection', function ($view) {
            $startDate = session()->get('startDate');
            $endDate = session()->get('endDate');

            $view->with(['startDate' => $startDate, 'endDate' => $endDate]);
        });
    }
}