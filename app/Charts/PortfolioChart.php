<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\Portfolio;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use Session;


class PortfolioChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $data = Session::get('portfolioData');

        $labelsFinal = array_map(function($item){
            return $item['report_date'];
        }, $data);

        $revenue = array_map(function($item){
            return ['y' => $item['revenue']];
        }, $data);

        $spend = array_map(function($item){
            return ['y' => $item['spend']];
        }, $data);

        $acos = array_map(function($item){
            return ['y' => $item['acos']];
        }, $data);

        $roas = array_map(function($item){
            return ['y' => $item['roas']];
        }, $data);

        $orders = array_map(function($item){
            return ['y' => $item['orders']];
        }, $data);

        $impressions = array_map(function($item){
            return ['y' => $item['impressions']];
        }, $data);

        return Chartisan::build()
            ->labels($labelsFinal)
            ->dataset('Revenue', $revenue)
            ->dataset('Spend', $spend)
            ->dataset('ACoS', $acos)
            ->dataset('RoAS', $roas)
            ->dataset('Orders', $orders)
            ->dataset('Impressions', $impressions)
            ;
    }
}