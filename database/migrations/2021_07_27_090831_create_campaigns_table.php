<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->id();
            $table->string('portfolio_id');
            $table->string('profile_id');
            $table->string('name');
            $table->double('spend');
            $table->integer('orders');
            $table->double('revenue');
            $table->double('acos');
            $table->double('roas');
            $table->integer('impressions');
            $table->integer('clicks');
            $table->double('costperclick');
            $table->double('clickthroughrate');
            $table->double('conversionrate');
            $table->date('report_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
