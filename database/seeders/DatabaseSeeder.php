<?php

namespace Database\Seeders;

use App\Models\Adgroup;
use App\Models\Campaign;
use App\Models\Client;
use App\Models\Keyword;
use App\Models\Productad;
use App\Models\Portfolio;
use App\Models\Profile;
use App\Models\Searchterm;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\DateFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\Sequence;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $client = Client::factory(5)->create();

        foreach ($client as $cl){
            $profile = Profile::factory(2)->create(['client_id' => $cl->id]);

            foreach ($profile as $pr){
                for($i=1; $i<=2; $i++){
                    $portfolio = Portfolio::factory(14)
                    ->state(new Sequence(
                        fn ($sequence) => [
                            'report_date' => now()->subDays($sequence->index)
                        ]
                    ))
                    ->create(['profile_id' => $pr->id, 'name' => 'PF_'.$i.'_'.$pr->name.'_'.$pr->countrycode]);
                    //56 Portfolios * 2

                    //create 14 campaigns for every group of portfolios with same name
                    $portfolioFinal = $portfolio->groupBy('name');
                    foreach ($portfolioFinal as $pf) {
                        for ($j = 1; $j <= 2; $j++) {
                            $campaign = Campaign::factory(14)
                            ->state(new Sequence(
                                    fn($sequence) => [
                                        'report_date' => now()->subDays($sequence->index)
                                    ]
                            ))
                            ->create(['portfolio_id' => $pf[0]->id, 'profile_id' => $pr->id, 'name' => 'CP_' . $j . '_' . $pf[0]->name]);

                            //create 14 ad groups for every group of campaigns with same name
                            $campaignFinal = $campaign->groupBy('name');
                            foreach ($campaignFinal as $ca) {
                                for ($k = 1; $k <= 2; $k++) {
                                    $adgroup = Adgroup::factory(14)
                                        ->state(new Sequence(
                                            fn($sequence) => [
                                                'report_date' => now()->subDays($sequence->index)
                                            ]
                                        ))
                                        ->create(['campaign_id' => $ca[0]->id, 'profile_id' => $pr->id, 'name' => 'AG_' . $k . '_' . $ca[0]->name]);

                                    //create 14 keywords for every group of ad groups with same name
                                    $adgroupFinal = $adgroup->groupBy('name');
                                    foreach ($adgroupFinal as $ag) {
                                        for ($l = 1; $l <= 2; $l++) {
                                            $keyword = Keyword::factory(14)
                                                ->state(new Sequence(
                                                    fn($sequence) => [
                                                        'report_date' => now()->subDays($sequence->index)
                                                    ]
                                                ))
                                                ->create(['adgroup_id' => $ag[0]->id, 'profile_id' => $pr->id, 'name' => 'KW_' . $l . '_' . $ag[0]->name]);

                                            //create 14 searchterms for every group of keywords with same name
                                            $keywordFinal = $keyword->groupBy('name');
                                            foreach ($keywordFinal as $kw) {
                                                for ($o = 1; $o <= 2; $o++) {
                                                    Searchterm::factory(14)
                                                        ->state(new Sequence(
                                                            fn($sequence) => [
                                                                'report_date' => now()->subDays($sequence->index)
                                                            ]
                                                        ))
                                                        ->create(['keyword_id' => $kw[0]->id, 'profile_id' => $pr->id, 'name' => 'ST_' . $o . '_' . $kw[0]->name]);
                                                }
                                            }
                                        }
                                    }

                                    //create 14 products for every group of ad groups with same name
                                    foreach ($adgroupFinal as $ag) {
                                        for ($l = 1; $l <= 2; $l++) {
                                            $product = Productad::factory(14)
                                                ->state(new Sequence(
                                                    fn($sequence) => [
                                                        'report_date' => now()->subDays($sequence->index)
                                                    ]
                                                ))
                                                ->create(['adgroup_id' => $ag[0]->id, 'profile_id' => $pr->id, 'name' => 'PR_' . $l . '_' . $ag[0]->name]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
