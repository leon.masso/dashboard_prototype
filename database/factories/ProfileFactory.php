<?php

namespace Database\Factories;

use App\Models\Profile;
use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Profile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'client_id' => Client::factory(),
            'name' => function (array $attributes) {
                return Client::find($attributes['client_id'])->name;
            },
            'countrycode' => $this->faker->countryCode(),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
