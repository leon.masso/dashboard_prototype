<?php

namespace Database\Factories;

use App\Models\Adgroup;
use App\Models\Campaign;
use Illuminate\Database\Eloquent\Factories\Factory;

class AdgroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Adgroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'campaign_id' => Campaign::factory(),
            'profile_id' => Campaign::factory(),
            'name' => Campaign::factory(),
            'spend' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 5000, $max = 50000),
            'orders' => $this->faker->numberBetween($min = 5000, $max = 10000),
            'revenue' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 5000, $max = 50000),
            'acos' => $this->faker->numberBetween($min = 5, $max = 500),
            'roas' => $this->faker->numberBetween($min = 1, $max = 500),
            'impressions' => $this->faker->numberBetween($min = 1000, $max = 50000),
            'clicks' => $this->faker->numberBetween($min = 100, $max = 50000),
            'costperclick' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0.01, $max = 10),
            'clickthroughrate' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'conversionrate' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'report_date' => '',
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
