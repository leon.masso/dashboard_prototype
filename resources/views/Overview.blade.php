@extends('layouts.master')
@section('title', 'Overview')
@php($currentPage = 'keywords')

@section('content')
    @if($message)
        <h1>{{ $message }}</h1>
    @endif
    @if($profile)
        @foreach($profile as $p)
            <h1 class="mb-4">Overview for {{ $p->name }} <span data-flag=@if($p->countrycode == 'UK') 'GB' @else {{ $p->countrycode }}@endif></span></h1>
            <h2>ID: {{ $p->profileid }}</h2>
        @endforeach
    @endif
@endsection