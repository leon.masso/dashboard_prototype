<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">




    <!-- *** CSS *** -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <!-- jQuery Datatables CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.25/af-2.3.7/b-1.7.1/b-colvis-1.7.1/b-html5-1.7.1/b-print-1.7.1/cr-1.5.4/date-1.1.0/fc-3.3.3/fh-3.1.9/kt-2.6.2/r-2.2.9/rg-1.1.3/rr-1.2.8/sc-2.0.4/sb-1.1.0/sp-1.3.0/sl-1.3.3/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/datetime/1.1.0/css/dataTables.dateTime.min.css"/>

    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <!-- Own CSS -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">




    <!-- *** JS *** -->

    <!-- jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- jQuery Datatables JS -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.25/af-2.3.7/b-1.7.1/b-colvis-1.7.1/b-html5-1.7.1/b-print-1.7.1/cr-1.5.4/date-1.1.0/fc-3.3.3/fh-3.1.9/kt-2.6.2/r-2.2.9/rg-1.1.3/rr-1.2.8/sc-2.0.4/sb-1.1.0/sp-1.3.0/sl-1.3.3/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/datetime/1.1.0/js/dataTables.dateTime.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.11.3/api/sum().js"></script>
    <script type="text/javascript" src="https:////cdn.datatables.net/plug-ins/1.11.3/api/average().js"></script>

    <!-- Charting library -->
    <script src="https://unpkg.com/chart.js@2.9.3/dist/Chart.min.js"></script>

    <!-- Chartisan -->
    <script src="https://unpkg.com/@chartisan/chartjs@^2.1.0/dist/chartisan_chartjs.umd.js"></script>

    <!--Own JS-->
    <script defer src="{{ asset('js/main.js') }}" type="text/javascript"></script>

    <title>@yield('title')</title>
</head>
<body>
    <div id="loading">
        <img id="loading-image" src="img/loader.gif" alt="Loading..." />
    </div>

    <nav id="topNav" class="navbar navbar-expand-sm bg-light navbar-light fixed-top shadow-sm">
        <a class="navbar-brand" href="/">
            <img src="{{ asset('/img/logoNav.png') }}" style="width: 115px;">
        </a>
        <ul class="ml-auto navbar-nav">
            <li class="nav-item {{ request()->is('overview') || request()->is('/') ? 'active' : '' }}">
                <a class="nav-link" href="/overview">Overview</a>
            </li>
            <li class="nav-item {{ request()->is('portfolios') ? 'active' : '' }}">
                <a class="nav-link" href="/portfolios">Portfolios</a>
            </li>
            <li class="nav-item {{ request()->is('campaigns') ? 'active' : '' }}">
                <a class="nav-link" href="/campaigns">Campaigns</a>
            </li>
            <li class="nav-item {{ request()->is('adgroups') ? 'active' : '' }}">
                <a class="nav-link" href="/adgroups">Ad Groups</a>
            </li>
            <li class="nav-item {{ request()->is('targets') ? 'active' : '' }}">
                <a class="nav-link" href="/targets">Targets</a>
            </li>
            <li class="nav-item {{ request()->is('searchterms') ? 'active' : '' }}">
                <a class="nav-link" href="/searchterms">Searchterms</a>
            </li>
            <li class="nav-item {{ request()->is('skus') ? 'active' : '' }}">
                <a class="nav-link" href="/skus">SKUs</a>
            </li>
            <li class="nav-item {{ request()->is('asins') ? 'active' : '' }}">
                <a class="nav-link" href="/asins">ASINs</a>
            </li>
        </ul>
    </nav>
    <div class="container mx-0">
        <div class="row">
            <div class="col-3 px-0">
                <!-- Sidebar -->
                <nav id="sidebar" class="bg-dark navbar-dark sticky-top">
                    <form action="/setProfile" method="post">
                        @csrf
                        <input type="hidden" name="selectedProfile" value="" id="selectedProfile"/>
                        <ul class="list-unstyled pt-5">
                            {{--@foreach($clients as $client)
                                <li>
                                    <a href="#{{ $client->name }}-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">{{ $client -> name }}</a>
                                    <ul id="{{ $client->name }}-Submenu" class="collapse list-unstyled">
                                        @foreach($client->profiles as $profile)--}}
                                        @foreach($profiles as $profile)
                                            <li>
                                                <a class="ClientProfile {{ $profile == $currentProfile[0] ? 'active' : '' }}" data-value="{{ $profile->profileid }}" >{{ $profile->name }} <span data-flag=@if($profile->countrycode == 'UK') 'GB' @else {{ $profile->countrycode }}@endif></span></a>
                                            </li>
                                        @endforeach
                                    {{--</ul>
                                </li>
                            @endforeach--}}
                        </ul>
                    </form>
                </nav>
            </div>
            <div class="col-9 px-0">
                <!-- Page Content -->
                <div id="content">
                    <div class="container-fluid p-4 pt-5 mt-4">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>