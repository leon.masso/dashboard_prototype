@extends('layouts.master')
@section('title', 'SKUs Overview')
@php($currentPage = 'skus')

@section('content')
    @foreach($profile as $p)
        <h1 class="mb-4">SKUs for {{ $p->name }} <span data-flag=@if($p->countrycode == 'UK') 'GB' @else {{ $p->countrycode }}@endif></span></h1>

        @include('components.dateSelection')

        @include('components.entitySelection')

        @include('components.entityData')

        <table class="dataTable table table-striped table-sm table-bordered">
            <thead>
            <tr>
                <th class="count">SKU</th>
                <th>Campaigns</th>
                <th class="sum">Impressions</th>
                <th class="sum">Clicks</th>
                <th id="cost" class="sum">Cost</th>
                <th class="sum">Orders</th>
                <th id="sales" class="sum">Sales</th>
                <th id="acos">ACOS</th>
            </tr>
            </thead>
            <tbody>
            @foreach($skus as $sku)
                <tr>
                    <td><a href="{{ $sku->sku }}">{{ $sku->sku }}</a></td>
                    <td class="text-right"><a href="/campaigns?entity=skus&skus={{ $sku->sku }}">{{ $sku->campaigns }}</a></td>
                    <td class="text-right">{{ $sku->impressions }}</td>
                    <td class="text-right">{{ $sku->clicks }}</td>
                    <td class="text-right">{{ $sku->cost }}</td>
                    <td class="text-right">{{ $sku->orders }}</td>
                    <td class="text-right">{{ $sku->sales }}</td>
                    <td class="text-right">{{ $sku->acos }}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
            </tr>
            </tfoot>
        </table>
    @endforeach
@endsection