@extends('layouts.master')
@section('title', 'Ad Groups Overview')
@php($currentPage = 'adgroups')

@section('content')
    @foreach($profile as $p)
        <h1 class="mb-4">Ad Groups for {{ $p->name }} <span data-flag=@if($p->countrycode == 'UK') 'GB' @else {{ $p->countrycode }}@endif></span></h1>

        @include('components.dateSelection')

        @include('components.entitySelection')

        @include('components.entityData')

        <table class="dataTable table table-striped table-sm table-bordered">
            <thead>
            <tr>
                <th class="count">Ad Group</th>
                <th>Campaign</th>
                <th>Camapigntype</th>
                <th class="sum">Impressions</th>
                <th class="sum">Clicks</th>
                <th id="cost" class="sum">Cost</th>
                <th class="sum">Orders</th>
                <th id="sales" class="sum">Sales</th>
                <th id="acos">ACOS</th>
            </tr>
            </thead>
            <tbody>
            @foreach($adgroups as $adgroup)
                <tr>
                    <td><a href="{{ $adgroup->adgroupid }}">{{ $adgroup->name }}</a></td>
                    <td>{{ $adgroup->campaignname }}</td>
                    <td>{{ $adgroup->campaigntype }}</td>
                    <td class="text-right">{{ $adgroup->impressions }}</td>
                    <td class="text-right">{{ $adgroup->clicks }}</td>
                    <td class="text-right">{{ $adgroup->cost }}</td>
                    <td class="text-right">{{ $adgroup->orders }}</td>
                    <td class="text-right">{{ $adgroup->sales }}</td>
                    <td class="text-right">{{ $adgroup->acos }}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
            </tr>
            </tfoot>
        </table>
    @endforeach
@endsection