@extends('layouts.master')
@section('title', 'Campaigns Overview')
@php($currentPage = 'campaigns')

@section('content')
    @foreach($profile as $p)
        <h1 class="mb-4">Campaigns for {{ $p->name }} <span data-flag=@if($p->countrycode == 'UK') 'GB' @else {{ $p->countrycode }}@endif></span></h1>

        @include('components.dateSelection')

        @include('components.entitySelection')

        @include('components.entityData')


        <table class="dataTable table table-striped table-sm table-bordered">
            <thead>
            <tr>
                <th class="count">Campaign</th>
                <th>Camapigntype</th>
                <th class="sum">Impressions</th>
                <th class="sum">Clicks</th>
                <th id="cost" class="sum">Cost</th>
                <th class="sum">Orders</th>
                <th id="sales" class="sum">Sales</th>
                <th id="acos">ACOS</th>
            </tr>
            </thead>
            <tbody>
            @foreach($campaigns as $campaign)
                <tr>
                    <td><a href="{{ $campaign->campaignid }}">{{ $campaign->name }}</a></td>
                    <td>{{ $campaign->campaigntype }}</td>
                    <td class="text-right">{{ $campaign->impressions }}</td>
                    <td class="text-right">{{ $campaign->clicks }}</td>
                    <td class="text-right">{{ $campaign->cost }}</td>
                    <td class="text-right">{{ $campaign->orders }}</td>
                    <td class="text-right">{{ $campaign->sales }}</td>
                    <td class="text-right">{{ $campaign->acos }}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
            </tr>
            </tfoot>
        </table>
    @endforeach
@endsection