@extends('layouts.master')
@section('title', 'ASINs Overview')
@php($currentPage = 'asins')

@section('content')
    @foreach($profile as $p)
        <h1 class="mb-4">ASINs for {{ $p->name }} <span data-flag=@if($p->countrycode == 'UK') 'GB' @else {{ $p->countrycode }}@endif></span></h1>

        @include('components.dateSelection')

        @include('components.entitySelection')

        @include('components.entityData')


        <table class="dataTable table table-striped table-sm table-bordered">
            <thead>
            <tr>
                <th class="count">ASIN</th>
                <th>Campaigns</th>
                <th class="sum">Impressions</th>
                <th class="sum">Clicks</th>
                <th id="cost" class="sum">Cost</th>
                <th class="sum">Orders</th>
                <th id="sales" class="sum">Sales</th>
                <th id="acos">ACOS</th>
            </tr>
            </thead>
            <tbody>
            @foreach($asins as $asin)
                <tr>
                    <td><a href="{{ $asin->asin }}">{{ $asin->asin }}</a></td>
                    <td class="text-right"><a href="/campaigns?entity=asins&asins={{ $asin->asin }}">{{ $asin->campaigns }}</a></td>
                    <td class="text-right">{{ $asin->impressions }}</td>
                    <td class="text-right">{{ $asin->clicks }}</td>
                    <td class="text-right">{{ $asin->cost }}</td>
                    <td class="text-right">{{ $asin->orders }}</td>
                    <td class="text-right">{{ $asin->sales }}</td>
                    <td class="text-right">{{ $asin->acos }}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
            </tr>
            </tfoot>
        </table>
    @endforeach
@endsection