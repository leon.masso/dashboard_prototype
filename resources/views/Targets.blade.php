@extends('layouts.master')
@section('title', 'Targets Overview')
@php($currentPage = 'targets')

@section('content')
    @foreach($profile as $p)
        <h1 class="mb-4">Targets for {{ $p->name }} <span data-flag=@if($p->countrycode == 'UK') 'GB' @else {{ $p->countrycode }}@endif></span></h1>

        @include('components.dateSelection')

        @include('components.entitySelection')

        @include('components.entityData')

        <table class="dataTable table table-striped table-sm table-bordered">
            <thead>
                <tr>
                    <th class="count">Target</th>
                    <th>Campaign</th>
                    <th>Camapigntype</th>
                    <th class="sum">Impressions</th>
                    <th class="sum">Clicks</th>
                    <th id="cost" class="sum">Cost</th>
                    <th class="sum">Orders</th>
                    <th id="sales" class="sum">Sales</th>
                    <th id="acos">ACOS</th>
                </tr>
            </thead>
            <tbody>
            @foreach($targets as $target)
                <tr>
                    <td><a href="{{ $target->targetid }}">{{ $target->targetingexpression }}</a></td>
                    <td>{{ $target->campaignname }}</td>
                    <td>{{ $target->campaigntype }}</td>
                    <td class="text-right">{{ $target->impressions }}</td>
                    <td class="text-right">{{ $target->clicks }}</td>
                    <td class="text-right">{{ $target->cost }}</td>
                    <td class="text-right">{{ $target->orders }}</td>
                    <td class="text-right">{{ $target->sales }}</td>
                    <td class="text-right">{{ $target->acos }}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th class="text-right"></th>
                    <th class="text-right"></th>
                    <th class="text-right"></th>
                    <th class="text-right"></th>
                    <th class="text-right"></th>
                    <th class="text-right"></th>
                </tr>
            </tfoot>
        </table>
@endforeach
@endsection