@isset($corEntities)
    <h4>Selected <strong>{{ $corEntityType }}</strong>:</h4>
    <div class="mb-3" style="width: 75vW;">
        @foreach($corEntities as $corEntity)
                @csrf
                <input type="hidden" name="selectedEntity" value="{{ $corEntity }}"/>
                <span class="badge badge-pill badge-warning p-3 my-1">
                <strong>{{ $corEntity }}</strong>
                <button type="button" onclick="removeQueryParam('{{ $corEntity }}')" class="close">&times</button>
            </span>
        @endforeach
    </div>
@endisset