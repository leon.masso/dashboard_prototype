<form id="corEntityForm" class="corEntity" method="post">
    @csrf
    <div class="input-group w-50 float-left">
        <input type="hidden" name="currentPage" value="{{ $currentPage }}">
        <select class="form-control" name="entitySelect">
            <option selected disabled>Select corresponding data</option>
            <option @if($currentPage == 'portfolios') disabled @endif value="portfolios">Portfolios</option>
            <option @if($currentPage == 'campaigns') disabled @endif value="campaigns">Campaigns</option>
            <option @if($currentPage == 'adgroups') disabled @endif value="adgroups">Ad Groups</option>
            <option @if($currentPage == 'targets') disabled @endif value="targets">Targets</option>
            <option @if($currentPage == 'searchterms') disabled @endif value="searchterms">Search Terms</option>
            <option @if($currentPage == 'asins') disabled @endif value="asins">ASINs</option>
            <option @if($currentPage == 'skus') disabled @endif value="skus">SKUs</option>
        </select>
        <div class="input-group-append">
            <button class="btn btn-info" type="button" onclick="getRelatedData()">Show me the money</button>
        </div>
    </div>
</form>