<form id="dateForm" class="dateRange mx-auto" action="/setDates" method="post">
    @csrf
    <div class="input-group mb-3  w-50 float-left">
        <div class="input-group-prepend">
            <span class="input-group-text">Start Date</span>
        </div>
        <input type="text" class="form-control dateInput startDate" name="startDate" value="{{ $startDate }}">
        <div class="input-group-prepend">
            <span class="input-group-text">End Date</span>
        </div>
        <input type="text" class="form-control dateInput endDate" name="endDate" value="{{ $endDate }}">
    </div>
</form>