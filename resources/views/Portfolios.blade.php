@extends('layouts.master')
@section('title', 'Portfolios Overview')
@php($currentPage = 'portfolios')

@section('content')
    @foreach($profile as $p)
        <h1 class="mb-4">Portfolios for {{ $p->name }} <span data-flag=@if($p->countrycode == 'UK') 'GB' @else {{ $p->countrycode }}@endif></span></h1>

        @include('components.dateSelection')

        @include('components.entitySelection')

        @include('components.entityData')

        <table class="dataTable table table-striped table-sm table-bordered">
            <thead>
            <tr>
                <th class="count">Portfolio</th>
                <th>Campaigns</th>
                <th class="sum">Impressions</th>
                <th class="sum">Clicks</th>
                <th id="cost" class="sum">Cost</th>
                <th class="sum">Orders</th>
                <th id="sales" class="sum">Sales</th>
                <th id="acos">ACOS</th>
            </tr>
            </thead>
            <tbody>
            @foreach($portfolios as $portfolio)
                <tr>
                    <td><a href="{{ $portfolio->portfolioid }}">{{ $portfolio->name }}</a></td>
                    <td class="text-right"><a href="/campaigns?entity=portfolios&portfolios={{ $portfolio->portfolioid }}">{{ $portfolio->campaigns }}</a></td>
                    <td class="text-right">{{ $portfolio->impressions }}</td>
                    <td class="text-right">{{ $portfolio->clicks }}</td>
                    <td class="text-right">{{ $portfolio->cost }}</td>
                    <td class="text-right">{{ $portfolio->orders }}</td>
                    <td class="text-right">{{ $portfolio->sales }}</td>
                    <td class="text-right">{{ $portfolio->acos }}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
            </tr>
            </tfoot>
        </table>
    @endforeach
@endsection