@extends('layouts.master')
@section('title', 'Searchterms Overview')
@php($currentPage = 'searchterms')

@section('content')
    @foreach($profile as $p)
        <h1 class="mb-4">Searchterms for {{ $p->name }} <span data-flag=@if($p->countrycode == 'UK') 'GB' @else {{ $p->countrycode }}@endif></span></h1>

        @include('components.dateSelection')

        @include('components.entitySelection')

        @include('components.entityData')

        <table class="dataTable table table-striped table-sm table-bordered">
            <thead>
            <tr>
                <th class="count">Target</th>
                <th>Search Term</th>
                <th>Campaign</th>
                <th>Camapigntype</th>
                <th>Ad Group</th>
                <th class="sum">Impressions</th>
                <th class="sum">Clicks</th>
                <th id="cost" class="sum">Cost</th>
                <th class="sum">Orders</th>
                <th id="sales" class="sum">Sales</th>
                <th id="acos">ACOS</th>
            </tr>
            </thead>
            <tbody>
            @foreach($searchterms as $searchterm)
                <tr>
                    <td><a href="{{ $searchterm->targetid }}">{{ $searchterm->target }}</a></td>
                    <td>{{ $searchterm->searchterm }}</td>
                    <td>{{ $searchterm->campaignname }}</td>
                    <td>{{ $searchterm->campaigntype }}</td>
                    <td>{{ $searchterm->adgroupname }}</td>
                    <td class="text-right">{{ $searchterm->impressions }}</td>
                    <td class="text-right">{{ $searchterm->clicks }}</td>
                    <td class="text-right">{{ $searchterm->cost }}</td>
                    <td class="text-right">{{ $searchterm->orders }}</td>
                    <td class="text-right">{{ $searchterm->sales }}</td>
                    <td class="text-right">{{ $searchterm->acos }}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
                <th class="text-right"></th>
            </tr>
            </tfoot>
        </table>
    @endforeach
@endsection